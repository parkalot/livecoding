describe("Canary test group", () => {
  it("Passing test", () => {
    const a = 2,
      b = 2;

      const c = a + b

      expect(c).toBe(4)
  });

  // it("Failing test", () => {
  //   const a = 2,
  //     b = 2;

  //     const c = a + b

  //     expect(c).toBe(3)
  // });
});
